/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/

function totalSum(num1, num2) {
	let sum = num1 + num2;
	
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum);

}

totalSum(5,15);

function totalDifference(num1, num2) {
	
	let difference = num2 - num1;
	
	console.log("Displayed sum of " + num2 + " and " + num1);
	console.log(difference);
}

totalDifference(5,20);

/*
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/

function totalProduct(num1, num2) {
	console.log("The product of " + num1 + " and " + num2);
	let times = num1 * num2;
	return times;
}

let multiply = totalProduct(50, 10);

console.log(multiply);

function totalQuotient(num1, num2) {
	console.log("The quotient of " + num1 + " and " + num2);
	let divide = num1 / num2;
	return divide;
}

let division = totalQuotient(50, 10);

console.log(division);

/*
	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console. Area of a circle = π × r2  pi = 3.14159
*/

function areaOfCircle(radius) {
	console.log("The result of getting the area of a circle with " + radius + ":");
	let pi = 3.1416
	let diameter = pi * (radius ** 2);
	return diameter;

}

let circleArea = areaOfCircle(15);
console.log(circleArea);


/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/

function totalAverage(num1, num2, num3, num4) {
	console.log("The average of " + num1 + "," + num2 + ","+ num3 + " and " + num4 + ":")
	let totalSum = num1 + num2 + num3 + num4;
	let totalLength = totalAverage.length;
	let average = totalSum / totalLength;
	return average; 
}

let averageVar =  totalAverage(20,40,60,80);
console.log(averageVar);

/*	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


function totalScore(totalCorrect, totalQuestion) {
	console.log("Is " + totalCorrect + "/" + totalQuestion + " a passing score?")
	let totalPercentage = totalCorrect / totalQuestion * 100;
	let isPassed = totalPercentage > 75;
	return isPassed;
}

let isPassingScore = totalScore(38, 50);
console.log(isPassingScore);